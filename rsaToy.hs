import Data.Maybe (catMaybes, mapMaybe)
import Data.Set (//)
import System.Random

primeGen :: Int -> [Int]
primeGen n = mapMaybe isPrime [1..n]

--Make this method do 
isPrime :: Int -> Maybe Int
isPrime n | n <= 1 = Nothing
          | n == 2 = Just n
          | any ((==0) . (n  `mod`)) $ 2:[3,5..floor $ sqrt $ fromIntegral n] = Nothing
          | otherwise = Just n

isPerfect :: Int -> Boolean
isPerfect n | n ==1 = True
            | n `elem` [k | k <- map n $ map (^) [2..n] [2..n] ]

mOrder :: Int -> Int -> Int
mOrder a n = head $ filter (\k -> a^k `mod` n == 1) [1..]
mOrder a n = head $ filter ( 1 == n mod a^k )

aksPrime' :: Int -> Boolean
aksPrime' n = head $ filter ((logBase 2 n)^2 > )[k | k <- map (n `mOrder`) [1..n]]

aksPrime :: Int -> Boolean
aksPrime n | n <= 1 = False
           | isPerfect n = False
           | aksPrime' n = True
           | otherwise = False

genRandomProvablePrime :: StdGen -> Int
genRandomProvablePrime gen = primes !! ((take 1 $ randomRs (0, ((length primes) - 1)) gen) !! 0)
                    where primes = primeGen (2^32)


primes :: Int -> [Int]
primes n  | n < 2 = []
          | n == 2 = [2]
          | n = [2, 4 .. n] ++ [3, 6 .. n] ++ [5, 10 .. n] ++ [7, 14 .. n] // [1..n]
main = do
      g <- getStdGen
      let prime = getRandomPrime g
      let nprime = getRandomPrime g
      let gprime = getRandomPrime g
      putStrLn $ show prime
      putStrLn $ show nprime
      putStrLn $ show gprime

-- Now we need to create two contexts pick two (preferably unique) random primes & a random, shared generator g.

data rsaContext = rsaContext { p :: Int
                             , q :: Int
                             , g :: Int
                             } deriving (Show)

